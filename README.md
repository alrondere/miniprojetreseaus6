# Mini-projet réseau - C
- BIZET Eric
- GARCIA Julien
- LE BERRIGAUD Mathis
- TREMILLON Xavier

Le projet rendu permet de simuler un anneau à n machines connectés les unes à la suite des autres.

## Compiler le projet

Pour compiler, lancer simplement la commande  `make` en étant placé dans le répertoire du projet.

## Lancer le projet
Lors de l'exécution, veiller à bien démarrer la machine d'identifiant `a` en dernier afin qu'elle puisse envoyer le token à toutes les autres. Un identifiant est un simple caractère qui identifiera par la suite la machine sur réseau en anneau.

### En local
Si le projet est lancé en local, les terminaux peuvent communiquer entre eux en envoyant les paquets sur LOCALHOST. Les ports utilisées par les n terminaux sont configurés en fonction de l'ID de chaque terminal (ex : a, b, c, d...).

Lorsque le running mode est demandé, entrer `l` pour local. L'identifiant du terminal est ensuite demandé.

Par défaut, le programme est configuré pour fonctionner avec 3 terminaux (qui peuvent s'appeler a, b et c par exemple). Pour changer ce nombre, il faut changer la constante ligne 20 dans **main.c**

### En réseau
Pour lancer entre plusieurs machines distinctes, lorsque le running_mode est demandé, entrer `n` pour networking.

Attention à bien remplir le fichier **config** comme présenté dans **config_example**. Le programme va ensuite se lancer directement en chargeant les paramètres du fichier **config**. Chaque machine doit avoir un fichier de configuration différent.
