#include "conn.h"

struct ports
{
    int reception;
    int emission;
};

//Generate a receiving and sending port based on the machine's name. The aim is to have a circular network.
struct ports generateLocalPorts(char machine, int n_machine)
{
  struct ports p;
  //If we are the last machine, then our sending port will be the port of machine A
  if((machine - 'a') + 1 == n_machine)
  {
      p.reception = BASE_PORT + (machine - 'a');
      p.emission = BASE_PORT;
  }
  //Else our sending port is the following
  else
  {
      p.reception = BASE_PORT + (machine - 'a');
      p.emission = p.reception + 1;
  }

  return p;
}

struct ports generateNetworkPorts()
{
  struct ports p;
  p.reception = BASE_PORT;
  p.emission = BASE_PORT;
  return p;
}


void connectToLocalNetwork(int* priseReception, int* priseEmission, char* machineId, int n_machine)
{
  printf("Running on local machine only ! \n");
  printf("Enter machine's id \n");
  //Getting the host name from the shell
  (*machineId) = enterChar();
  struct ports p = generateLocalPorts((*machineId), n_machine);
  (*priseReception) = creerPriseReception(p.reception);
  (*priseEmission) = creerPriseEmission(LOCALHOST, p.emission);
  printf("Machine %c is ready ! \n", (*machineId));
}

void connectToNetwork(int* priseReception, int* priseEmission, char* machineId, char* nextAddr)
{
  //Chargement de nextAddr et machineId via fonction Mathis
  readConfig(machineId, nextAddr);
  printf("Running on network ! \n");
  printf("Next computer address is : %s \n", nextAddr);
  struct ports p = generateNetworkPorts();
  (*priseReception) = creerPriseReception(p.reception);
  (*priseEmission) = creerPriseEmission(nextAddr, p.emission);
  printf("Machine %c is ready ! \n", (*machineId));
}

void sendFirstPacket(char* machineId, struct Packet* pack, char* buffer, int* priseEmission)
{
  //Telling that the packet contains information
  pack->vide = 1;
  //Setting the target of the first message, here computer B
  pack->source = (*machineId);
  //Setting the sender of the first message, here computer A
  printf("Enter first message receiver \n");
  pack->dest = enterChar();
  //Store the first message
  strcpy(pack->message, "I start the conversation");
  //Encode packet data into a buffer to be sent
  encode(pack, buffer);
  //send the buffer
  envoie((*priseEmission), buffer, SIZE_PACKET);
  //make sure we won't do the first transmission multiple times
}

int creerPriseEmission (char *server, int port)
{
    int			sock;
    struct sockaddr_in	address;
    struct hostent     *hp;

   /*
   * Cr�ation d'une socket UDP :
   * PF_INET : famille de protocoles Internet
   * SOCK_DGRAM : communication par datagrammes
   * 0 : protocole UDP implicite (car inet + dgram)
   */
    if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) == -1)
    {
    	perror ("creerPriseEmission");
    	exit (1);
    }

   /*
   * R�solution d'adresse � partir du nom de l'h�te
   */
    hp = gethostbyname(server);
    if (hp == 0)
    {
    	perror ("creerPriseEmission");
    	exit (1);
    }

   /*
   * Cr�ation d'une structure d'adresse de socket
   *
   * sin_family = AF_INET : famille d'adresses Internet
   *
   * sin_addr.s_addr : on r�cup�re l'adresse donn�e par gethostbyname
   *
   * sin_port = htons (port) : port destinataire,
   *   conversion au format reseau (big endian) du num�ro de port,
   *   htons : host to network short integer)
   */
    memset (&address, 0, sizeof (address));
    memcpy (&address.sin_addr, hp->h_addr, hp->h_length);
    address.sin_family = AF_INET;
    address.sin_port = htons (port);

 /*
 * connect pour les sockets UDP permet d'affecter l'adresse destinataire
 * qui sera utilisee lors d'une emission avec send
 */
    if (connect (sock,
		 (struct sockaddr*) &address,
		 sizeof (struct sockaddr_in)) == -1)
    {    return 0;

    	perror ("creerPriseEmission");
    	exit (1);
    }

    return sock;
}


int creerPriseReception (int port)
{
    int	sock;
    struct sockaddr_in	address;
/*
 * Cr�ation d'une socket UDP :
 * PF_INET : famille de protocoles Internet
 * SOCK_DGRAM : communication par datagrammes
 * 0 : protocole UDP implicite (car Inet + Dgram)
 */
    if ((sock = socket (PF_INET, SOCK_DGRAM, 0)) == -1)
    {
	perror ("creerPriseReception");
    	exit (1);
    }

   /*
   * Cr�ation d'une structure d'adresse de socket
   *
   * sin_family = AF_INET : famille d'adresses Internet
   *
   * sin_addr.s_addr = INADDR_ANY : l'adresse de r�ception n'est pas sp�cifi�e
   *   (la requ�te peut aboutir sur n'importe quelle interface locale)
   *
   * sin_port = htons (port) : le port de r�ception est sp�cifi�,
   *   conversion au format reseau (big endian) du num�ro de port,
   *   htons : host to network short integer)
   */
    memset (&address, 0, sizeof (address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons (port);

   /*
   * Affectation d'une adresse locale � la socket
   */
    if (bind (sock,
	      (struct sockaddr*) &address,
	      sizeof (address)) == -1)
    {
    	perror ("creerPriseReception");
    	exit (1);
    }

    return sock;
}

int envoie (int prise, char *buffer, ssize_t taille)
{
    if (write (prise, buffer, taille) == taille)
	    return 0;
    else
	   return -1;
}

int recoit (int prise, char *buffer, size_t taille)
{
    if (read (prise, buffer, taille) >= 0)
      return 0;
    else
      return -1;
}
