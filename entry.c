#include "entry.h"


char enterChar()
{
    char c = getchar(); //Get the character
    getchar(); //Skip the \n
    return c; //Return value
}

void enterMessage(int length, char* array_to_fill)
{
    int count = 0;
    char c;

    //While the string is not done
    while((c = getchar()) != '\n')
    {
        //If we can store the entered char
        if(count < length-1)
        {
          //Store char in the array
          array_to_fill[count] = c;
          count++;
        }
    }
    //At the end we add the \0 characters
    array_to_fill[count] = 0;
    //We fill the rest of the array with \0 characters
    for(int i = count+1; i < length; i++)
    {
      array_to_fill[i] = 0;
    }

}

void readConfig(char* id, char* ip)
{
  char currentChar;

  FILE *fp;
  fp = fopen("config", "r");

  if (fp == NULL)
  {
        printf ("No config file found\n");
        exit(EXIT_FAILURE);
  }
  else
  {
    currentChar = getc(fp);
    *id = currentChar;
    if(getc(fp) != '\n')
    {
      printf("Uncorrect config file\n");
      exit(EXIT_FAILURE);
    }
    else
    { int i=0;
      while(((currentChar = getc(fp)) != '\n') && (i<(IPLENGTH-1)))
      {
        ip[i]=currentChar;
        i++;
      }
      ip[i+1]='\0';
    }
  }
  fclose (fp);
}
