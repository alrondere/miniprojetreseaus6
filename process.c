#include "process.h"


void process(Packet* p, char* received_buffer, char id)
{
    printf("------------------\n");
    //Decode the data from the buffer
    decode(received_buffer, p);

    //send the packet through layers
    layer_1_up(p, id);

    //encode the resulting packet p into a the buffer
    encode(p, received_buffer);
    printf("------------------\n\n");
}

void layer_1_up(Packet *p, char id)
{
    //Printing when a token arrives
    printf("A token arrived !\n");
    //Send every token to the second layer
    layer_2_up(p, p->vide, id);
}

void layer_1_down(Packet *p, int has_to_send_something)
{
  //Setting the empty information of the packet before forwarding it.
  p->vide = has_to_send_something;
}


void layer_2_up(Packet *p, int contains_something, char id)
{
    //If the packet we received is not empty
    if(contains_something)
    {
        //In the case where we received a packet send from us to us. No receipt acknowledgment, just send back an empty token
        if(p->dest == p->source && p->dest == id)
        {
            printf("Packet received from myself !\n");
            //Stay in the layer 2, do not go until 3rd layer
            layer_2_down(p, 0, 0, '0', '0');
        }
        else if(p->dest == id)
        {
            //Here we receive a message from someone who is not us
            printf("Message from : %c\n", p->source);
            //Send the data to the 3rd layer to read the message content.
            layer_3(p, 1, id);

        }
        else if(p->source == id)
        {
            if(p->receipt_validation == 0)
            {
              //In this case, the receipt validation field has never been switched to 1 by a receiver
              printf("unknown host : %c \n", p->dest);
              layer_2_down(p, 0, 0, '0', '0');
            }
            else if(p->receipt_validation == 1)
            {
              //In this case, we received a receipt acknowledgment, we just need to forward an empty token
              printf("receipt acknowledgment from : %c\n", p->dest);
              layer_2_down(p, 0, 0, '0', '0');
            }
        }
        else
        {
            //In this case, the message is not for us, we just forward it, keeping the source and dest the same as they were.
            layer_2_down(p, 1, 0, p->source, p->dest);
        }
    }
    else //If the packet is empty we send it to the 3rd layer in order to maybe put a message in it
    {
        layer_3(p, 0, id);
    }

}

void layer_2_down(Packet *p, int has_to_send_something, int send_accuse, char src, char dest)
{
  //If we want to send a receipt acknowledgment
  if(send_accuse == 1)
  {
    //We do not change anything to the packet
    p->receipt_validation = 1;
    layer_1_down(p, 1);
  }
  else
  {
    //If we have something to send
    if(has_to_send_something == 1)
    {
      p->dest = dest;
      p->source = src;
      resetReceiptValidation(p);
      layer_1_down(p, 1);
    }
    //If we do not have anything to send
    else if(has_to_send_something == 0)
    {
      //reset the addresses. We are in layer2 so we shoud not be able here to modify a message content or to say if the packet is empty or not we just have access to adresses.
      resetAddr(p);
      layer_1_down(p, 0);
    }
  }
}


void layer_3(Packet *p, int contains_something, char id)
{
    if(contains_something)
    {
        //If we have a message to read, then we display it and forward a receipt acknowledgment
        printf("Content : %s \n", p->message);
        layer_2_down(p, 1, 1, '0', '0');
    }
    else
    {
        //Handling the messaging part
        printf("Do you want to send a message ? ( y / n) ?\n");
        char on = enterChar();
        if(on == 'y')
        {
            printf("Please type the message : \n");
            //storing the entered message into the packet's variable
            enterMessage(SIZE_MSG, p->message);
            //Asking for the recipient machine
            printf("Who is the recipient : \n");
            char dest = enterChar();
            //go back to 2nd layer
            layer_2_down(p, 1, 0, id, dest);
        }
        else
        {
            //If we do not want to say anything, we just reset the message's content and go back to 2nd layer
            resetMessage(p);
            layer_2_down(p, 0, 0, '0', '0');
        }
    }
}
