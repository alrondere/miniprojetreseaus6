#ifndef _H_INCLUDED
#define _H_INCLUDED

#include "packet.h"
#include "entry.h"

//Function to call once a buffer is received.
//It will fill the packet with the information and will then use it through the different layers declared under.
//In the following functions, id is always the "name" of the current machine, like 'a', 'b', 'c', ...
void process(Packet* p, char* received_buffer, char id);

//Simulate our first layer's behavior.
void layer_1_up(Packet *p, char id);
void layer_1_down(Packet *p, int has_to_send_something);

//Simulate our second layer's behavior
void layer_2_up(Packet *p, int contains_something, char id);
void layer_2_down(Packet *p, int has_to_send_something, int send_accuse, char src, char dest);

//Simulate our third layer's behavior
void layer_3(Packet *p, int contains_something, char id);

#endif
