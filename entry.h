#ifndef ENTRY_H_INCLUDED
#define ENTRY_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#define IPLENGTH 16
//Get a single char from command line
char enterChar();

//Get a string with a maximal length from command line
void enterMessage(int length, char* array_to_fill);

void readConfig(char* id, char* ip);

#endif
