#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "conn.h"
#include "packet.h"
#include "process.h"

int main()
{
    int priseReception = 0;
    int priseEmission = 0;
    char host_name = ' ';

    //Asking for network or local test
    printf("Enter running mode \n");
    char run_mode = enterChar();
    if(run_mode == 'l')
    {
      int n_machine_in_local = 3;
      connectToLocalNetwork(&priseReception, &priseEmission, &host_name, n_machine_in_local);
    }
    else if(run_mode == 'n')
    {
      //Getting the host name from the shell
      char nextAddr[IPLENGTH];
      connectToNetwork(&priseReception, &priseEmission, &host_name, nextAddr);
    }


    //The buffer in which we will receive data and then fill with new data to forward.
    char buffer[SIZE_PACKET];
    //Packet used to decode data and use them easily
    Packet pack;
    //Creating the first packet in order to start the transmission through the network
    if(host_name == 'a')
    {
      sendFirstPacket(&host_name, &pack, buffer, &priseEmission);
    }
    //Infinite loop, a user should not lost connection or the network is done
    while(1)
    {
        //Receive data (blocking function)
        recoit(priseReception, buffer, SIZE_PACKET);
        //process data once we receive them
        process(&pack, buffer, host_name);
        //Forward the result : a new message ? An empty token ? A receipt acknowledgment ?
        envoie(priseEmission, buffer, SIZE_PACKET);
        //reset the packet and buffer's content
        resetPacket(&pack);
        memset(buffer, 0, SIZE_PACKET);
    }
}
