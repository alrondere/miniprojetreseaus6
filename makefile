CC = gcc
FLAGS = -W -Wall

all : projet_reseau
	make clean

projet_reseau: main.o conn.o entry.o packet.o process.o
	$(CC) -o projet_reseau main.o conn.o entry.o packet.o process.o

main.o: main.c conn.h packet.h process.h
	$(CC) -o main.o -c main.c $(FLAGS)

conn.o: conn.c conn.h entry.h packet.h
	$(CC) -o conn.o -c conn.c $(FLAGS)

entry.o: entry.c entry.h
	$(CC) -o entry.o -c entry.c $(FLAGS)

packet.o: packet.c packet.h
	$(CC) -o packet.o -c packet.c $(FLAGS)

process.o: process.c packet.h entry.h
	$(CC) -o process.o -c process.c $(FLAGS)

clean:
	rm -f *.o
