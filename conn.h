#ifndef CONN_H_INCLUDED
#define CONN_H_INCLUDED

#include <stdio.h>	// perror
#include <netdb.h>	// socket, connect, hostent
#include <arpa/inet.h>  // sockaddr_in
#include <stdlib.h>
#include <unistd.h>
#include <string.h>	// memset

#include "entry.h"
#include "packet.h"

//useful defines
#define LOCALHOST "127.0.0.1"
#define BASE_PORT 3000

//Local and online port generation functions
struct ports;
struct ports generateLocalPorts(char machine, int n_machine);
struct ports generateNetworkPorts();

//Used to connect on a local or online network
void connectToLocalNetwork(int* priseReception, int* priseEmission, char* machineId, int n_machine);
void connectToNetwork(int* priseReception, int* priseEmission, char* machineId, char* nextAddr);

void sendFirstPacket(char* machineId, struct Packet* pack, char* buffer, int* priseEmission);

//teacher's function
int creerPriseEmission (char *server, int port);
int creerPriseReception (int port);

int envoie(int prise, char *buffer, ssize_t taille);
int recoit(int prise, char *buffer, size_t taille);

#endif
