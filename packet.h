#ifndef PACKET_H
#define PACKET_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define SIZE_EMPTY sizeof(int)
#define SIZE_ADDR sizeof(char)
#define SIZE_RECEIPT_VALIDATION sizeof (int)
#define LENGTH_MESSAGE 100
#define SIZE_MSG LENGTH_MESSAGE*sizeof(char)
#define SIZE_PACKET SIZE_EMPTY+SIZE_ADDR*2+SIZE_RECEIPT_VALIDATION+SIZE_MSG

//packet structure
typedef struct Packet{
    int vide;
    char dest;
    char source;
    int receipt_validation;
    char message[LENGTH_MESSAGE];
} Packet;

//Reset all packets values
void resetPacket(Packet *p);
//Reset only message content value
void resetMessage(Packet *p);
//reset the addr stored in packet
void resetAddr(Packet *p);
//reset receipt validation
void resetReceiptValidation(Packet *p);

//Encode a packet in a buffer, make sure the buffer is the right size
void encode(Packet *p, char* buffer);
//Decode from a buffer to a packet
void decode(char* buf, Packet* p);

//Functions mainly used to debug the code
void printBuffer(char* buf);
void printPacket(Packet *p);

#endif
