#include "packet.h"

void resetPacket(Packet *p)
{
  p->vide = 0;
  //use of already existing functions :
  resetAddr(p);
  resetMessage(p);
  resetReceiptValidation(p);
}

void resetMessage(Packet *p)
{
    //set all the message characters to \0 to prevent any segmentation fault
    for(int i = 0; i < LENGTH_MESSAGE; i++)
        p->message[i] = 0;
}

void resetAddr(Packet *p)
{
    p->dest = '0';
    p->source = '0';
}

void resetReceiptValidation(Packet *p)
{
  p->receipt_validation = 0;
}

void encode(Packet *p, char* buffer)
{
    //using defines to fill the buffer from packet's information, to understand refeer to the packet Struct declared in packet.h
    buffer[0] = (p->vide == 0) ? 0 : 1;
    buffer[SIZE_EMPTY] = p->dest;
    buffer[SIZE_EMPTY + SIZE_ADDR] = p->source;
    buffer[SIZE_EMPTY + SIZE_ADDR*2] = p->receipt_validation;
    memcpy(buffer + (SIZE_EMPTY + SIZE_ADDR*2 + SIZE_RECEIPT_VALIDATION), p->message, SIZE_MSG);
}

void decode(char* buf, Packet* p)
{
    //using defines to fill a packet from buffer's information, to understand refeer to the packet Struct declared in packet.h
    p->vide = buf[0];
    p->dest = buf[SIZE_EMPTY];
    p->source = buf[SIZE_EMPTY + SIZE_ADDR];
    p->receipt_validation = buf[SIZE_EMPTY + SIZE_ADDR*2];
    memcpy(p->message, buf + (SIZE_EMPTY + SIZE_ADDR*2 + SIZE_RECEIPT_VALIDATION), SIZE_MSG);
}

void printBuffer(char* buf)
{
    printf("%d ", buf[0]);
    printf("%c ", buf[SIZE_EMPTY]);
    printf("%c ", buf[SIZE_EMPTY + SIZE_ADDR]);
    printf("%d ", buf[SIZE_EMPTY + SIZE_ADDR*2]);
    char* c = buf + SIZE_EMPTY + SIZE_ADDR*2 + SIZE_RECEIPT_VALIDATION;
    for(int i = 0; i < LENGTH_MESSAGE; i++)
      printf("%c", *(c+i));
    printf("\n");
}

void printPacket(Packet *p)
{
    printf("Vide : %d, Dest : %c, Source : %c, Receipt_validation : %d, Msg : %s \n", p->vide, p->dest, p->source, p->receipt_validation, p->message);
}
